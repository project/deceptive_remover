CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a Drush command to identify and remove any malware and phishing code 
injected into the site after a cyber attack. The module looks for any unsafe code injected 
into text area fields in entities and helps to remove them.

 * For a full description of the module visit:
   https://www.drupal.org/project/deceptive_remover

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/deceptive_remover


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Deceptive Site module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/docs/7/extend/installing-modules for
   further information.


CONFIGURATION
-------------

This module uses drush commands so no configuration is needed.
   

MAINTAINERS
-----------

 * Heshan Wanigasooriya (heshanlk) - https://www.drupal.org/u/heshanlk

Need help?

Contact me here https://www.heididev.com/hire
