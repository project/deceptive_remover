<?php

/**
 * Implements hook_drush_command().
 */
function deceptive_remover_drush_command() {

  $commands['remove-unsafe-code'] = array(
    'description' => 'This command will loop throught all the nodes and their fields (textfields) and check for any unsafe code.',
    'aliases' => array('ruc'),
     'examples' => array(
       'drush ruc' => 'Start the command.',
     ),
  );

  return $commands;
}

/**
 * Drush command logic.
 * drush_[COMMAND_NAME]().
 */
function drush_deceptive_remover_remove_unsafe_code() {
  $types = node_type_get_types();
  $options = array();
  foreach($types as $machine_name => $type){
    $options[$machine_name] = $type->name;
  }
  $content_type = drush_choice($options, dt('Choose a content type to start?'));
  // load nodes
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $content_type)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (!empty($result)) {
    // Load field instances
    $fields = array();
    $instances = field_info_instances('node', $content_type);

    // Fields.
    foreach ($instances as $name => $instance) {
      $field = field_info_field($instance['field_name']);
      if(in_array($field['type'], array('text', 'text_long', 'text_with_summary'))){
        $fields[$instance['field_name']] = array(
          'title' => $instance['label'],
          'weight' => $instance['widget']['weight'],
        );
      }
    }
    // Load nodes
    $news_items_nids = count(array_keys($result['node']));
    if (drush_confirm('Are you sure you want continue scan ' . $news_items_nids . ' of ' . $content_type . '?')) {
      foreach($result['node'] as $nid => $node){
        $node = node_load($nid);
        $wrapper = entity_metadata_wrapper('node', $node);
        foreach($fields as $field_machine_name => $field){
          if ($wrapper->__isset($field_machine_name)) {
            $field_value = $wrapper->{$field_machine_name}->value();
          }
          else {
            continue;
          }
          
          if(empty($field_value['value'])) {
            continue;
          }
          
          // $field_value = $wrapper->{$field_machine_name}->value->value();
          preg_match('/<script.*>.*<\/script>/ims', $field_value['value'], $matches);
          if(empty($matches)){
            continue;
          }
          else {
            $new_field_value = preg_replace('/<script.*>.*<\/script>/ims', "", $field_value['value']);
            if(!empty($new_field_value)){
              $wrapper->{$field_machine_name}->set(array('value' => $new_field_value, 'format' => $field_value['format']));
              $wrapper->save();
              drush_log(dt('Node scanned and unsecure code found!', array()), 'success');
            }
          }
        }
      }
    }
    else {
      drush_user_abort();
    }
  }
  else {
    drush_log(dt('Nothing found!', array()), 'success');
  }
}